FROM node:16.16-alpine AS base
WORKDIR /usr/src/app
RUN chown node:node -R /usr/src/app
COPY --chown=node:node . .

FROM base AS prod
RUN npm install --production
USER node
EXPOSE 3000
CMD [ "npm", "start" ]
