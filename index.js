require("dotenv").config();

const express = require("express");
const axios = require("axios");
const app = express();
const port = process.env.PORT || 3000;

app.get("/ip", async (req, res) => {
  try {
    const result = await axios.get("https://ifconfig.me/ip");
    return res.json({
      ipAddress: result.data,
    });
  } catch (err) {
    console.log(JSON.stringify(err));
    return res.json({
      error: {
        message: err.message,
        stack: err.stack,
      },
    });
  }
});

app.listen(port, () => {
  console.log(`Environment: ${process.env.NODE_ENV}`);
  console.log(`Server listening on port: ${port}`);
});
